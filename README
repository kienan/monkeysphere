The monkeysphere puppet module is designed to help you manage your servers
and users using the monkeysphere[0].

To install the monkeypshere module, storeconfigs should be enabled in
your puppet server to use certain features. See:

http://projects.puppetlabs.com/projects/1/wiki/Using_Stored_Configuration#Configuring+basic+storeconfigs

Example usage for server setup:

  # Assuming you are using the sshd puppet module...
  $sshd_authorized_keys_file = "/var/lib/monkeysphere/authorized_keys/%u"
  include sshd

  # Optionally, indicate your preferred keyserver. You can specify a server
  # under your control and not accessible to the public or
  # pool.sks-keyservers.net if you want to publish to the public pool. The
  # value you specify here will be used for all monkeysphere and gpg commands
  $monkeysphere_keyserver = "zimmermann.mayfirst.org" 
  include monkeysphere

  # Ensure the server's ssh key is imported into your monkeysphere key ring
  monkeysphere::import_key { "main": }

  # Optionally publish the server key to a keyserver (as indicated above)
  monkeysphere::publish_server_keys { "main": } 
  
  # Optionally email the server key to your self
  monkeysphere::email_server_keys { "we@ourdomain.org": }

  # Be sure to sign the server's key!

  # Indiciate the fingerprint of the gpg key that should be used
  # to verify user ids. You can repeat this for as many certifiers
  # as you need
  monkeysphere::add_id_certifier { "jamie":
	  keyid => "1CB57C59F2F42470238F53ABBB0B7EE15F2E4935" 
  }
  
  # Indicate who should have root access on the server 
  monkeysphere::authorized_user_ids { "root":
	  user_ids => [ "sarah <sarah@ourgroup.org>" , "jose <josue@ourgroup.org" ] 
  } 

In addition, you may want to create a password-less key for a user to use
when logging into another server (e.g. if you want automated backups from
one server to another).

Example usage for user setup:

  # Ensure that the root user has authentication capable
  # monkeysphere key 
  monkeysphere::auth_capable_user { "root": }

  # Optionally publish the key
  monkeysphere::publish_user_key { "root": } 

  # Grant full trust to a gpg key so the root user can properly
  # authenticate servers to which it connects
  # You can run this as many times as you want
  monkeysphere::owner_trust { "jamie":
	  fingerprint => "0EE5BE979282D80B9F7540F1CCD2ED94D21739E9" 
  }

A host can be configured as a host you would use to sign the gpg keys by placing:

  include monkeysphere::signer

into the node definition. ON this host, a file will be placed in
/var/lib/puppet/modules/monkeysphere/hosts for each host configured as a
sshserver.  Each file will contin the gpg id, the gpg fingerprint, and
the ssh fingerprint of the sshserver.

0. http://monkeysphere.info/
