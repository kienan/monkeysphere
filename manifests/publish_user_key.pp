define monkeysphere::publish_user_key ( ){
  $user = $title

  $keyserver_arg = $monkeysphere_keyserver ? {
    '' => '',
    default => "--keyserver $monkeysphere_keyserver"
  }

  exec { "monkeysphere-gpg-send-key-$user":
    command => "gpg $keyserver_arg --send-key $(gpg --list-secret-key --with-colons | grep ^sec | cut -d: -f5)",
    require => [ Package["monkeysphere"], Exec["monkeysphere-gen-key-$user" ] ],
    user => $user,
  }

}
