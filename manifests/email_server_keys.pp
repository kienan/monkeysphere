# optionally, mail key somehwere 
define monkeysphere::email_server_keys ( ) {
  $email = $title    
  exec { "mail -s 'monkeysphere host pgp keys for $fqdn' $email < /var/lib/monkeysphere/host_keys.pub.pgp":
    require     => Package["monkeysphere"],
    subscribe   => Exec["monkeysphere-import-key"],
    refreshonly => true,
  }
}
