# ensure that the user has a gpg key created and it is authentication capable
# in the monkeysphere. This is intended to be the same as generated a
# password-less ssh key 
#
define monkeysphere::auth_capable_user (
  $expire = "1y",
  $length = "2048", 
  $uid_name = undef,
  $email = undef ) { 

  $user = $title

  # The goal is no passphrase, monkeysphere won't work without a passphrase. 
  $calculated_passphrase = $gpg_auto_password ? {
    '' => 'monkeys',
    default => $gpg_auto_password
  }

  $calculated_name = $uid_name ? {
    '' => "$user user",
    default => $uid_name
  }
  $calculated_email = $email ? {
    '' => "$user@$fqdn",
    default => $email
  }
  exec { "monkeysphere-gen-key-$user":
    command => "printf 'Key-Type: RSA\nKey-Length: 2048\nKey-Usage: encrypt,sign\nSubkey-Type: RSA\nSubkey-Length: 2048\nSubkey-Usage: encrypt\nName-Real: $calculated_name\nName-Email: $calculated_email\nPassphrase: $calculated_passphrase\nExpire-Date: 1y\n' | gpg --batch --gen-key",
    require => [ Package["monkeysphere"] ],
    user => $user,
    unless => "gpg --list-secret-key | grep ^sec >/dev/null"
  }

  #FIXME - we should check expiration date and extend it if we're < n days before expiration

  # handle auth subkey
  exec { "monkeysphere-gen-subkey-$user":
    command => "printf '$calculated_passphrase\n' | monkeysphere gen-subkey",
    require => [ Package["monkeysphere"], Exec["monkeysphere-gen-key-$user" ] ],
    user => $user,
    unless => "gpg --list-key --with-colons $(gpg --list-secret-key --with-colons | grep ^sec | cut -d: -f5) | grep ^sub | cut -d: -f12 | grep a >/dev/null"
  }

}
