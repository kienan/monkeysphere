# This module is distributed under the GNU Affero General Public License:
#
# Monkeysphere module for puppet
# Copyright (C) 2009-2010 Sarava Group
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#
# Class for monkeysphere management
#

class monkeysphere(
  $ssh_port       = '',
  $ensure_version = 'installed',
  # if not false, will override the path for MONKEYSPHERE_RAW_AUTHORIZED_KEYS
  # use 'none' to disable appending the authorized_keys file
  # see monkeysphere-authentication for more information
  $raw_authorized_keys = false,
  $keyserver      = 'pool.sks-keyservers.net'
) {
  # The needed packages
  package { 'monkeysphere':
    ensure => $ensure_version,
  }

  $key = "ssh://${::fqdn}${port}"

  modules_dir { [ 'monkeysphere', 'monkeysphere/hosts', 'monkeysphere/plugins' ]: }

  file {
    # This was the old way which the module checked monkeysphere keys
    '/usr/local/sbin/monkeysphere-check-key':
      ensure  => absent,
      owner   => root,
      group   => root,
      mode    => 0755,
      content => "#!/bin/bash\n/usr/bin/gpg --homedir /var/lib/monkeysphere/host --list-keys '=$key' &> /dev/null || false";
    'monkeysphere_conf':
      path => '/etc/monkeysphere/monkeysphere.conf',
      mode => 644,
      ensure => present,
      content => template('monkeysphere/monkeysphere.conf.erb'),
      require => Package['monkeysphere'];
    'monkeysphere_host_conf':
      path => '/etc/monkeysphere/monkeysphere-host.conf',
      mode => 644,
      ensure => present,
      content => template('monkeysphere/monkeysphere-host.conf.erb'),
      require => Package['monkeysphere'];
    'monkeysphere_authentication_conf':
      path => '/etc/monkeysphere/monkeysphere-authentication.conf',
      mode => 644,
      ensure => present,
      content => template('monkeysphere/monkeysphere-authentication.conf.erb'),
      require => Package['monkeysphere'];
  }
}
